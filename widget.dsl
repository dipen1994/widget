  pipelineJob("iris-widgets") {
	description()
	keepDependencies(false)
	
	definition {
		cpsScm {
			scm {
				git {
					remote {
						url("https://dipen1994@bitbucket.org/dipen1994/widget.git")
                        credentials('none')
					}
                  branch('${ReleaseBranch}')
				}
			}
			scriptPath("Jenkinsfile")
		}
	}
    parameters {
      gitParam("ReleaseBranch") {
        description('Release tag or branch')
        defaultValue('master')
        type('BRANCH_TAG')
	  }
    choiceParam('OPTION', ['dev (default)', 'prod', 'qa'])
    }

	disabled(false)
    triggers {
        bitbucketPush()
    }
	configure {
		it / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' {
			'autoRebuild'('false')
			'rebuildDisabled'('false')
		}
	}
}
